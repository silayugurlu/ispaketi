package com.company.crawler.core;

import java.util.List;

import com.company.crawler.model.CrawlerUrl;

/**
 *
 * @author silay
 */
public interface Crawler {
    
   /**
    * find urls in the page given as a url parameter
    * 
    * @param url
    * @return list of urls
    */
   List<CrawlerUrl> crawl(CrawlerUrl url);
}
