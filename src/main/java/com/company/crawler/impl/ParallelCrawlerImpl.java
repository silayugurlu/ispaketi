package com.company.crawler.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.company.crawler.ParallelCrawler;
import com.company.crawler.core.JsoupCrawlerHelper;
import com.company.crawler.model.CrawlerUrl;
import com.company.crawler.task.CrawlerThread;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

/**
 *
 * @author silay
 */
public class ParallelCrawlerImpl implements ParallelCrawler {

    private JsoupCrawlerHelper crawler = new JsoupCrawlerHelper();

    @Override
    public Queue<CrawlerUrl> crawl(String url, int depth) {

        if (url == null || url.isEmpty() || depth < 0) {
            return null;
        }

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);

        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();

        //crawl first url
        CrawlerUrl crawlerUrl = new CrawlerUrl(url, 0);
        crawler.crawl(crawlerUrl, hazelcastInstance, depth);

        IQueue<CrawlerUrl> urlsToCrawl = hazelcastInstance.getQueue("urlsToCrawl");

        // while there is any threads are running or there exists urls to crawl
        while (hazelcastInstance.getQueue("urlsToCrawl").size() > 0 || executor.getActiveCount() > 0 || executor.getQueue().size() > 0) {

            urlsToCrawl = hazelcastInstance.getQueue("urlsToCrawl");

            if (urlsToCrawl.size() > 10) {
                // create new thread for 10 urls to crawl
                List<CrawlerUrl> urls = new ArrayList<CrawlerUrl>();

                // remove urls and put to list
                urlsToCrawl.drainTo(urls, 10);

                Runnable worker = new CrawlerThread(urls, depth, crawler);
                executor.submit(worker);
            }

            //get url from urlsToCrawl queue and crawl url
            CrawlerUrl urlToCrawl = urlsToCrawl.poll();
            crawler.crawl(urlToCrawl, hazelcastInstance, depth);
        }

        return hazelcastInstance.getQueue("crawledUrls");
    }

}
