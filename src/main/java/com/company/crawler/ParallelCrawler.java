
package com.company.crawler;

import java.util.Queue;

import com.company.crawler.model.CrawlerUrl;

/**
 *
 * @author silay
 */
public interface ParallelCrawler {
    
    /**
     * get url and depth , crawl it to the level of the given depth paralelly
     * @param url 
     * @param depth
     * @return crawled urls
     */
     Queue<CrawlerUrl> crawl(String url, int depth);
    
}
