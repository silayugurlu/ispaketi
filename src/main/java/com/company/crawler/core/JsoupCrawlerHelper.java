/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.crawler.core;

import java.util.List;

import com.company.crawler.model.CrawlerUrl;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

/**
 *
 * @author silay
 */
public class JsoupCrawlerHelper {

	private Crawler crawler = new JsoupCrawler();

	public void crawl(CrawlerUrl crawlerUrl, HazelcastInstance hazelcastInstance, int depth) {
		if (crawlerUrl != null) {
			List<CrawlerUrl> results = crawler.crawl(crawlerUrl); // crawl url
			crawlerUrl.setInstanceName(hazelcastInstance.getName());
			IQueue<CrawlerUrl> crawledUrls = hazelcastInstance.getQueue("crawledUrls");
			crawledUrls.add(crawlerUrl); // add url to crawled urls queue

			if (crawlerUrl.getDepth() < depth) { // if it reaches maximum depth
													// do not crawl urls in it
				for (CrawlerUrl result : results) {

					// get the latest crawledUrls and urlsToCrawl queue
					IQueue<CrawlerUrl> urlsToCrawl = hazelcastInstance.getQueue("urlsToCrawl");
					synchronized (urlsToCrawl) {
						crawledUrls = hazelcastInstance.getQueue("crawledUrls");

						// check if this url is crawled already or added to the
						// urlsToCrawl queue by another thread
						// if it is do not add urlsToCrawl queue

						if (!crawledUrls.contains(result) && !urlsToCrawl.contains(result)) {
							urlsToCrawl.add(result);// add url to urlsToCrawl
													// queue
						}
					}
				}
			}

		}

	}
}
