package com.company.crawler;

import com.company.crawler.impl.ParallelCrawlerImpl;
import com.company.crawler.model.CrawlerUrl;
import java.util.Queue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author silay
 */
public class ParallelCrawlerTest {

    @Test
    public void testParallelCrawl() {

        String url = "http://example.com/";
        int depth = 2;

        ParallelCrawler crawler = new ParallelCrawlerImpl();

        Queue<CrawlerUrl> results = crawler.crawl(url, depth);
        System.out.println("RESULTS: ");
        for (CrawlerUrl crawlerUrl : results) {
            System.out.println(crawlerUrl.toString());
            assertTrue("Exceeded given depth", crawlerUrl.getDepth() <= depth);
        }
    }
    
    
    @Test
    public void testParallelCrawlExceptional() {

        String url = "";
        int depth = -2;

        ParallelCrawler crawler = new ParallelCrawlerImpl();

        Queue<CrawlerUrl> results = crawler.crawl(url, depth);
        
        assertNull(results);
    }
    
    @Test
    public void testParallelCrawlExceptionalNull() {

        String url = null;
        int depth = 2;

        ParallelCrawler crawler = new ParallelCrawlerImpl();

        Queue<CrawlerUrl> results = crawler.crawl(url, depth);
        
        assertNull(results);
    }

}
