
package com.company.crawler.core;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.company.crawler.model.CrawlerUrl;

/**
 *
 * @author silay
 */
public class JsoupCrawler implements Crawler, Serializable{


    @Override
    public List<CrawlerUrl> crawl(CrawlerUrl url) {
        List<CrawlerUrl> urlsToCrawl = new ArrayList<CrawlerUrl>();
        try {

            Connection connection = Jsoup.connect(url.getUrl()); // connect url with jsoup

            Document doc = connection.get();   //download document from connection
            Elements linksOnPage = doc.select("a[href]"); //get links in document 

            int urlCount = linksOnPage.size();
            url.setUrlCount(urlCount); // set count of links in the page with given url
            int depth = url.getDepth() + 1;
            /*add each url to the list*/

            for (Element link : linksOnPage) {

                String hrefStr = link.absUrl("href");
                if (!hrefStr.isEmpty()) {
                    CrawlerUrl urlToCrawl = new CrawlerUrl();
                    urlToCrawl.setUrl(hrefStr);
                    urlToCrawl.setDepth(depth);
                    urlsToCrawl.add(urlToCrawl);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(JsoupCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return urlsToCrawl;
    }

}
