package com.company.crawler.task;

import java.io.Serializable;
import java.util.List;

import com.company.crawler.core.JsoupCrawlerHelper;
import com.company.crawler.model.CrawlerUrl;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

/**
 *
 * @author silay
 */
public class CrawlerThread implements Runnable, Serializable {

	private List<CrawlerUrl> urlsToCrawl;
	private int maximumDepth;
	private JsoupCrawlerHelper helper;

	public CrawlerThread(List<CrawlerUrl> urls, int depth, JsoupCrawlerHelper crawlerHelper) {

		this.urlsToCrawl = urls;
		this.maximumDepth = depth;
		this.helper = crawlerHelper;
	}

	@Override
	public void run() {

		HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();

		for (CrawlerUrl url : urlsToCrawl) {
			helper.crawl(url, hazelcastInstance, maximumDepth);
		}

		Thread.currentThread().interrupt();

	}
}
