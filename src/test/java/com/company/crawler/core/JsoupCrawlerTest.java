package com.company.crawler.core;

import com.company.crawler.model.CrawlerUrl;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author silay
 */
public class JsoupCrawlerTest {

    @Test
    public void testCrawl() {

        JsoupCrawler crawler = new JsoupCrawler();
        List<CrawlerUrl> urls = crawler.crawl(new CrawlerUrl("http://example.com/", 0));
        Assert.assertEquals(1, urls.size());

    }
}
